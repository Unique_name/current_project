<?php

namespace common\dto;

/**
 * Class BookDto
 *
 * @package common\dto
 */
final class BookDto
{
    /** @var integer $id */
    public $id;
    /** @var string $title */
    public $title;
    /** @var string $pub_date */
    public $pub_date;
    /** @var string publisher */
    public $publisher;
    /** @var integer author_id */
    public $author_id;
}