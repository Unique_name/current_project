<?php

namespace common\service;

use common\models\Book;
use api\models\BookCreateRequest;
use common\repository\BookRepository;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class BookService
 *
 * @package common\service
 */
class BookService
{
    /** @var BookRepository $bookRepository */
    protected $bookRepository;


    /**
     * BookService constructor.
     *
     * @param BookRepository $bookRepository
     */
    public function __construct(
        BookRepository $bookRepository
    ) {
      $this->bookRepository = $bookRepository;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getBooksList()
    {
        $books = $this->bookRepository->getBooksList();
        return $books;
    }

    /**
     * @param int $id
     *
     * @return Book
     */
    public function getBook(int $id)
    {
        $book = $this->bookRepository->get($id);
        return $book;
    }

    /**
     * @param BookCreateRequest $request
     *
     * @return Book
     * @throws BadRequestHttpException
     */
    public function create(BookCreateRequest $request)
    {
        $bookDto = $request->getBookDto();
        $book = Book::create($bookDto);
        if (!$this->bookRepository->save($book)) {
            throw new BadRequestHttpException('Bad data');
        }
        return $book;
    }

    /**
     * @param $id
     *
     * @return Book
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete($id)
    {
        $book = $this->bookRepository->get($id);
        if ($book == null){
            throw new NotFoundHttpException();
        }
        if ($this->bookRepository->delete($book) === false){
            throw new BadRequestHttpException('Bad request');
        }
        return $book;
    }


}