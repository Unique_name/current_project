<?php

namespace common\repository;

use common\models\Book;

use yii\db\Exception;
use yii\web\BadRequestHttpException;

/**
 * Class BookRepository
 *
 * @package common\repository
 */
class BookRepository
{
    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getBooksList()
    {
        return Book::find()->all();
    }

    /**
     * @param int|null $id
     *
     * @return Book
     */
    public function get(?int $id)
    {
        if ($id == null){
            return null;
        }

        return Book::findOne(['id' => $id]);
    }

    /**
     * @param Book $book
     *
     * @return bool
     */
    public function save(Book $book){
        return $book->save();
    }

    /**
     * @param Book $book
     *
     * @return false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete(Book $book){
        return $book->delete();
    }
}