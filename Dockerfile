FROM gitlab.icerockdev.com:4567/docker/dev-alpine-php-7.2:latest

WORKDIR /app

COPY . /app

RUN ls

RUN composer install && \
    /app/init --env=Development && \
    ls


