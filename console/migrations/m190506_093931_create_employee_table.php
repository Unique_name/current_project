<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee}}`.
 */
class m190506_093931_create_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(64)->notNull(),
            'branch_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-company',
            'employee',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-branch',
            'employee',
            'branch_id',
            'branch',
            'id',
            'CASCADE'
        );


        for ($ind = 1; $ind < 4; $ind++){
            for ($i = 0; $i < 4; $i++) {
                $branch_id = 1;
                if ($ind == 2) { $branch_id = 5; }
                if ($ind == 3) { $branch_id = 9; }
                $this->insert('employee', [
                    'first_name' => chr($ind + 97),
                    'branch_id' => random_int($branch_id, $branch_id + 3),
                    'company_id' => $ind,
                ]);
            }
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee}}');
    }
}
