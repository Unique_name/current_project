<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%branch}}`.
 */
class m190506_093910_create_branch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%branch}}', [
            'id' => $this->primaryKey(),
            'location' => $this->string(160)->notNull(),
            'employee_count' => $this->integer()->defaultValue(1),
            'company_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-company',
            'branch',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );


        for ($ind = 0; $ind < 12; $ind++){

            $company_id = 1;
            $location = 'stud';
            if ($ind > 3){ $company_id = 2; $location = 'okt'; }
            if ($ind > 7){ $company_id = 3; $location = 'len'; }

            $this->insert('branch', [
                'location' => $location .'-'. strval($ind),
                'employee_count' => 50,
                'company_id' => $company_id,
            ]);

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%branch}}');
    }
}
