<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company}}`.
 */
class m190506_092504_create_company_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    private $companies = ['Apple', 'Facebook', 'Valve'];

    public function safeUp()
    {
        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull()->unique(),
            'employee_count' => $this->integer()->defaultValue(1),
            'branch_count' => $this->integer()->defaultValue(1),
        ]);

        for ($ind = 0; $ind< count($this->companies); $ind++){
            $this->insert('company', [
               'name' => $this->companies[$ind],
               'employee_count' => 200,
               'branch_count' => 4,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company}}');
    }
}
