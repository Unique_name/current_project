<?php

namespace backend\controllers;

use Yii;
use backend\models\LoginForm;
use common\models\User;
use yii\data\SqlDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;


class SiteController extends Controller
{

    public $user = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider_apple = new SqlDataProvider([
            'sql' => 'SELECT first_name, location, name
                      From employee inner join branch on
                      employee.branch_id=branch.id inner join company on
                      employee.company_id=company.id where employee.company_id=1',
        ]);

        $dataProvider_all = new SqlDataProvider([
            'sql' => 'SELECT first_name, location, name
                      From employee inner join branch on
                      employee.branch_id=branch.id inner join company on
                      employee.company_id=company.id',
        ]);
        return $this->render('index',[
            'dataProviderApple' => $dataProvider_apple,
            'dataProviderAll' => $dataProvider_all,
            ]);
    }

}